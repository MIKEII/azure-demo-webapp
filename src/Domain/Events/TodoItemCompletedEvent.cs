﻿using AzureDemoWebApp.Domain.Common;
using AzureDemoWebApp.Domain.Entities;

namespace AzureDemoWebApp.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
