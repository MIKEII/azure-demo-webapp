﻿using AzureDemoWebApp.Domain.Common;
using AzureDemoWebApp.Domain.Entities;

namespace AzureDemoWebApp.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
