﻿using System;

namespace AzureDemoWebApp.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
