﻿using AzureDemoWebApp.Domain.Common;
using System.Threading.Tasks;

namespace AzureDemoWebApp.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
