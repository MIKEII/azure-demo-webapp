﻿using AzureDemoWebApp.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace AzureDemoWebApp.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
