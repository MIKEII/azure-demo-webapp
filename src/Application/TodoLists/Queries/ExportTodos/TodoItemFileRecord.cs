﻿using AzureDemoWebApp.Application.Common.Mappings;
using AzureDemoWebApp.Domain.Entities;

namespace AzureDemoWebApp.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
