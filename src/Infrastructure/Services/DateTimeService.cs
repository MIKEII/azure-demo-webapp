﻿using AzureDemoWebApp.Application.Common.Interfaces;
using System;

namespace AzureDemoWebApp.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
