﻿using Microsoft.AspNetCore.Identity;

namespace AzureDemoWebApp.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
